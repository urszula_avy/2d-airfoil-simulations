# Script for poostprocessing aerodynamic coefficients for 2D airfoil simulation
# Urszula Golyska 2021 c
import matplotlib.pyplot as plt

def loop_func(cl,cd,cm,dir_name):
    # open file
    post_file = open(dir_name+"/postProcessing/forcesCoeffs/0/forceCoeffs.dat", 'r')
    for line in reversed(open(dir_name+"/postProcessing/forcesCoeffs/0/forceCoeffs.dat").readlines()):
           if line!="":
               coeffs = line.split()
               cm.append(float(coeffs[1]))
               cd.append(float(coeffs[2]))
               cl.append(float(coeffs[3]))
               break

if __name__ == '__main__':
       # user input
       U = float(input("Enter freestream velocity to analyze (in m/s):"))
       alpha_start = float(input("Enter lowest angle of attack to analyze (in degrees):"))
       alpha_end = float(input("Enter highest angle of attack to analyze (in degrees):"))
       alpha_inc = float(input("Enter angle of attack increment (in degrees):"))

       alpha_array = [alpha_start + alpha_inc * interval for interval in range(int((alpha_end - alpha_start)/alpha_inc)+1)]

       cl = [];
       cd = [];
       cm = [];
       for alpha in alpha_array:
              dir_name = "U_" + str(U) + "_aoa_" + str(alpha)
              loop_func(cl,cd,cm,dir_name)

       # plotting
       # cl
       plt.figure(1)
       plt.plot(alpha_array, cl)
       plt.xlabel('Angle of attack $[^\circ]$')
       plt.ylabel('$C_L$ lift coefficient')
       plt.grid()
       plt.savefig('CL_'+"U_" + str(U)+'aoa'+str(alpha_start)+'to'+str(alpha_end)+'.png')
       plt.show()

       # cd
       plt.figure(2)
       plt.plot(alpha_array, cd)
       plt.xlabel('Angle of attack $[^\circ]$')
       plt.ylabel('$C_D$ drag coefficient')
       plt.grid()
       plt.savefig('CD_'+"U_" + str(U)+'aoa'+str(alpha_start)+'to'+str(alpha_end)+'.png')
       plt.show()

       # cm
       plt.figure(3)
       plt.plot(alpha_array, cm)
       plt.xlabel('Angle of attack $[^\circ]$')
       plt.ylabel('$C_M$ momentum coefficient')
       plt.grid()
       plt.savefig('CM_'+"U_" + str(U)+'aoa'+str(alpha_start)+'to'+str(alpha_end)+'.png')
       plt.show()
