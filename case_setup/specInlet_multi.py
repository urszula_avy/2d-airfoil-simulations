# Script for changing inlet conditions for 2D airfoil simulation
# Urszula Golyska 2021 c
import math
import os

def service_func(U, alpha, dir_name):
       Ux = U*math.cos(math.radians(alpha))
       Uy = U*math.sin(math.radians(alpha))

       # Change velocity file
       U_file = open(dir_name+"/0/U", 'r')
       velocity_file = open(dir_name+"/0/U_new", 'w')
       
       for line in U_file:
              if "internalField   uniform" in line:
                     s = "internalField   uniform ("+str(Ux)+" "+str(Uy)+" 0);\n"
                     velocity_file.writelines(s)
              else:
                     velocity_file.writelines(line)

       velocity_file.close()
       U_file.close()

       os.remove(dir_name+'/0/U')
       os.rename(dir_name+'/0/U_new', dir_name+'/0/U')

       # Change contolDict file
       oldDict_file = open(dir_name+"/system/controlDict", 'r')
       newDict_file = open(dir_name+"/system/controlDict_new", 'w')

       for line in oldDict_file:
              if "liftDir" in line:
                     s = "         liftDir   (" + str(-math.sin(math.radians(alpha))) + " " + str(math.cos(math.radians(alpha))) + " 0);\n"
                     newDict_file.writelines(s)
              elif "dragDir" in line:
                     s = "         dragDir   (" +str(math.cos(math.radians(alpha))) + " " + str(math.sin(math.radians(alpha))) + " 0);\n"
                     newDict_file.writelines(s)
              elif "magUInf" in line:
                     s = "         magUInf   " + str(U)+";\n"
                     newDict_file.writelines(s)
              elif "UInf" in line and not "UInf;" in line:
                     s = "         UInf   (" + str(Ux) + " "+ str(Uy)+ " 0);\n"
                     newDict_file.writelines(s)
              else:
                     newDict_file.writelines(line)

       oldDict_file.close()
       newDict_file.close()

       os.remove(dir_name+'/system/controlDict')
       os.rename(dir_name+'/system/controlDict_new', dir_name+'/system/controlDict')

if __name__ == '__main__':
       # user input
       U = float(input("Enter freestream velocity (in m/s):"))
       alpha = float(input("Enter angle of attack (in degrees):"))
       dir_name=''   # default directory in current folder
       
       service_func(U,alpha, dir_name)

