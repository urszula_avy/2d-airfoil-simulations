# Script for postprocessing aerodynamic coefficients for 2D airfoil simulation
# Urszula Golyska 2021 c
import matplotlib.pyplot as plt

def plotting_func():
    # xfoil data
    aoa_xfoil = []
    cl_xfoil = []
    cd_xfoil = []
    cm_xfoil = []

    # Spalart-Allmaras
    cl_SA = []
    cd_SA = []
    aoa_SA = []

    # k-omega
    cl_kom = []
    cd_kom = []
    aoa_kom = []

    # SA
    with open("cl_cd_SA.txt", "r") as file:
        for line in file.readlines():
            line = line.split()
            cd_SA.append(float(line[2]))
            cl_SA.append(float(line[1]))
            aoa_SA.append(float(line[0]))

    # k-omega
    with open("cl_cd_komega.txt", "r") as file:
        for line in file.readlines():
            line = line.split()
            cd_kom.append(float(line[2]))
            cl_kom.append(float(line[1]))
            aoa_kom.append(float(line[0]))

    # xfoil
    xfoil_file = open('JF254110_xfoil.txt', 'r')
    count = 0
    for line in reversed(open('JF254110_xfoil.txt').readlines()):
        if '--' in line:
            break
        else:
            coeffs = line.split()
            aoa_xfoil.append(float(coeffs[0]))
            cl_xfoil.append(float(coeffs[1]))
            cd_xfoil.append(float(coeffs[2]))
            cm_xfoil.append(float(coeffs[4]))
    xfoil_file.close()

    # plotting
    # cl
    plt.figure(1)
    plt.plot(aoa_kom, cl_kom, 'o-')
    plt.plot(aoa_SA, cl_SA, 'x-')
    plt.plot(aoa_xfoil, cl_xfoil, '--')
    plt.xlabel('Angle of attack $[^\circ]$')
    plt.ylabel('$C_L$ lift coefficient')
    plt.grid()
    plt.legend(['k-omega SST', 'Spalart-Allmaras', 'Xfoil'])
    plt.savefig('CL_SA_komega.png')
    plt.show()

    # cd
    plt.figure(2)
    plt.plot(aoa_kom, cd_kom, 'o-')
    plt.plot(aoa_SA, cd_SA, 'x-')
    plt.plot(aoa_xfoil, cd_xfoil, '--')
    plt.xlabel('Angle of attack $[^\circ]$')
    plt.ylabel('$C_D$ drag coefficient')
    plt.grid()
    plt.legend(['k-omega SST', 'Spalart-Allmaras', 'Xfoil'])
    plt.savefig('CD_SA_komega.png')
    plt.show()

    # cm
    # plt.figure(3)
    # plt.plot(alpha_array, cm)
    # plt.plot(aoa_xflr, cm_xflr)
    # plt.xlabel('Angle of attack $[^\circ]$')
    # plt.ylabel('$C_M$ momentum coefficient')
    # plt.grid()
    # plt.legend(['SA', 'k-omega', 'XFLR', 'Xfoil'])
    # plt.savefig('CM_' + "U_" + str(U) + 'aoa' + str(alpha_start) + 'to' + str(alpha_end) + '.png')
    # plt.show()


if __name__ == '__main__':
       plotting_func()