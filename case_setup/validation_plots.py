# Script for poostprocessing aerodynamic coefficients for 2D airfoil simulation
# Urszula Golyska 2021 c
import math

import matplotlib.pyplot as plt

def loop_func(cl,cd,cm,dir_name, U, alpha,rho,S):
    # open file
    post_file = open(dir_name+"/postProcessing/forces/0/forces.dat", 'r')
    for line in reversed(open(dir_name+"/postProcessing/forces/0/forces.dat").readlines()):
       if line!="":
           forces = line.split()
           #print(forces)
           forces[1] = forces[1].replace("(","")
           forces[4] = forces[4].replace("(", "")
           fx = float(forces[1]) + float(forces[4])
           fy = float(forces[2]) + float(forces[5])
           #print(fx, fy)
           lift = fy*math.cos(math.radians(alpha))-fx*math.sin(math.radians(alpha))
           drag = fy*math.sin(math.radians(alpha))+fx*math.cos(math.radians(alpha))
           #print(lift,drag)
           cl_loc = 2*lift/(rho*U**2*S)
           cd_loc = 2*drag/(rho*U**2*S)
           # cm_kom.append(float(coeffs[1]))
           cd.append(cd_loc)
           cl.append(cl_loc)
           break


def plotting_func(cl,cd,cm,alpha_array):
        # xfoil data
       aoa_xfoil = []
       cl_xfoil = []
       cd_xfoil = []
       cm_xfoil = []

       # cl
       xfoil_file = open('JF254110_xfoil.txt', 'r')
       count = 0
       for line in reversed(open('JF254110_xfoil.txt').readlines()):
              if '--' in line:
                     break
              else:
                     coeffs = line.split()
                     aoa_xfoil.append(float(coeffs[0]))
                     cl_xfoil.append(float(coeffs[1]))
                     cd_xfoil.append(float(coeffs[2]))
                     cm_xfoil.append(float(coeffs[4]))
       xfoil_file.close()


       # plotting
       # cl
       plt.figure(1)
       # plt.plot(alpha_array, cl_SA, 'o-')
       plt.plot(alpha_array, cl, 'x-')
       # plt.plot(aoa_xflr, cl_xflr, '-.')
       plt.plot(aoa_xfoil, cl_xfoil, '--')
       plt.xlabel('Angle of attack $[^\circ]$')
       plt.ylabel('$C_L$ lift coefficient')
       plt.grid()
       plt.legend(['OpenFOAM','Xfoil'])
       plt.savefig('CL_' + "U_" + str(U) + 'aoa' + str(alpha_start) + 'to' + str(alpha_end) + '.png')
       plt.show()

       # cd
       plt.figure(2)
       # plt.plot(alpha_array, cd_SA, 'o-')
       plt.plot(alpha_array, cd, 'x-')
       # plt.plot(aoa_xflr, cd_xflr, '-.')
       plt.plot(aoa_xfoil, cd_xfoil, '--')
       plt.xlabel('Angle of attack $[^\circ]$')
       plt.ylabel('$C_D$ drag coefficient')
       plt.grid()
       plt.legend(['OpenFOAM','Xfoil'])
       plt.savefig('CD_' + "U_" + str(U) + 'aoa' + str(alpha_start) + 'to' + str(alpha_end) + '.png')
       plt.show()

       # cm
       # plt.figure(3)
       # plt.plot(alpha_array, cm)
       # plt.plot(aoa_xflr, cm_xflr)
       # plt.xlabel('Angle of attack $[^\circ]$')
       # plt.ylabel('$C_M$ momentum coefficient')
       # plt.grid()
       # plt.legend(['SA', 'k-omega', 'XFLR', 'Xfoil'])
       # plt.savefig('CM_' + "U_" + str(U) + 'aoa' + str(alpha_start) + 'to' + str(alpha_end) + '.png')
       # plt.show()

if __name__ == '__main__':
       # user input
       U = float(input("Enter freestream velocity to analyze (in m/s):"))
       alpha_start = float(input("Enter lowest angle of attack to analyze (in degrees):"))
       alpha_end = float(input("Enter highest angle of attack to analyze (in degrees):"))
       alpha_inc = float(input("Enter angle of attack increment (in degrees):"))

       alpha_array = [alpha_start + alpha_inc * interval for interval in range(int((alpha_end - alpha_start)/alpha_inc)+1)]

       rho = 1.225;
       S = 1; #chord

       cl = [];
       cd = [];
       cm = [];

       for alpha in alpha_array:
              dir_name = "U_" + str(U) + "_aoa_" + str(alpha)
              loop_func(cl,cd,cm,dir_name,U, alpha,rho,S)

      # write results into file
       with open("cl_cd.txt", "w") as f:
              for i in range(len(alpha_array)):
                     f.write(str(alpha_array[i]) + " " + str(cl[i]) + " " + str(cd[i]) + "\n")

       plotting_func(cl,cd,cm,alpha_array)