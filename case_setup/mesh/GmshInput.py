# Script for generating Gmsh .geo file for generating the whole mesh
# Urszula Golyska 2021 c
import math
import XFLRtoGmsh

def service_func(airfoil_name, alpha, default_options):
       airfoil_file = airfoil_name + ".geo"
       try:
              f = open(airfoil_file)
       except IOError:
              XFLRtoGmsh.coordinate_conversion(airfoil_name)
       mesh_file = open(airfoil_name + "_mesh.geo", 'w')

       mesh_file.writelines("Include \"" + airfoil_file + "\";")

       for line in reversed(open(airfoil_file).readlines()):  # find number of points on airfoil
              if "Point" in line:
                     st = line.find("(")
                     end = line.find(")")
                     nr_points = int(line[st + 1:end])  # 209
                     break

       # where to split the airfoil - change manually!
       n3 = str(round(nr_points / 2 + 1))  # leading edge
       n1 = str(round(3 * nr_points / 8) + 2)
       n2 = str(round(5 * nr_points / 8) - 2)

       # domain size
       x_arc = -1.5
       y_domain = 5
       x_wake_fine = 5
       x_wake_coarse = 15

       # Mesh parameters
       n_inlet = 25
       n_outlet = 80
       n_airfoil = 60
       n_wake_short = 60
       n_wake_long = 30

       if not default_options:
           x_arc = input("x_arc (default -1.5):")
           y_domain = input("y_domain (default 5):")
           x_wake_fine = input("x_wake_fine (default 5):")
           x_wake_coarse = input("x_wake_coarse (default 15):")

           # Mesh parameters
           n_inlet = input("n_inlet (default 25):")
           n_outlet = input("n_outlet (default 80):")
           n_airfoil = input("n_airfoil (default 60):")
           n_wake_short = input("n_wake_short (default 60):")
           n_wake_long = input("n_wake_long (default 30):")

       body = "\nalpha = " + str(alpha) + "; //Angle of attack in degrees\n" \
              "Rotate {{0, 0, 1}, {0, 0, 0}, -Pi/180*alpha} {Curve{1};}\n" \
              "Split Curve(1) {" + n1 + ", " + n2 + ", 1, " + n3 + "};\n" \
              "\nPoint(" + str(nr_points + 2) + ") = {" + str(x_arc) + ", " + str(y_domain) + ", 0, 1.0};\n" \
              "Point(" + str(nr_points + 3) + ") = {" + str(x_arc) + ", " + str(-y_domain) + ", 0, 1.0};\n" \
              "Point(" + str(nr_points + 4) + ") = {" + str(-math.sqrt((1 - x_arc) ** 2 + y_domain ** 2) + 1) + ", 0, 0, 1.0};\n" \
              "\nCircle(7) = {" + str(nr_points + 2) + ", 2, " + str(nr_points + 4) + "};\n" \
              "Circle(8) = {" + str(nr_points + 3) + ", 2, " + str(nr_points + 4) + "};\n"
       mesh_file.writelines(body)

       body = "Point(" + str(nr_points + 5) + ") = {1, " + str(-y_domain) + ", 0, 1.0};\n" \
              "Point(" + str(nr_points + 6) + ") = {1, " + str(y_domain) + ", 0, 1.0};\n" \
              "Point(" + str(nr_points + 7) + ") = {" + str(x_wake_fine) + ", " + str(y_domain) + ", 0, 1.0};\n" \
              "Point(" + str(nr_points + 8) + ") = {" + str(x_wake_fine) + ", 0, 0, 1.0};\n" \
              "Point(" + str(nr_points + 9) + ") = {" + str(x_wake_fine) + ", " + str(-y_domain) + ", 0, 1.0};\n" \
              "Point(" + str(nr_points + 10) + ") = {" + str(x_wake_coarse) + ", " + str(-y_domain) + ", 0, 1.0};\n" \
              "Point(" + str(nr_points + 11) + ") = {" + str(x_wake_coarse) + ", 0, 0, 1.0};\n" \
              "Point(" + str(nr_points + 12) + ") = {" + str(x_wake_coarse) + ", " + str(y_domain) + ", 0, 1.0};\n" \
              "\nLine(9) = {" + str(nr_points + 2) + ", " + n1 + "};\n" \
              "Line(10) = {" + str(nr_points + 3) + ", " + n2 + "};\n" \
              "Line(11) = {" + str(nr_points + 2) + ", " + str(nr_points + 6) + "};\n" \
              "Line(12) = {" + str(nr_points + 6) + ", " + str(nr_points + 7) + "};\n" \
              "Line(13) = {" + str(nr_points + 7) + ", " + str(nr_points + 12) + "};\n" \
              "Line(14) = {" + str(nr_points + 3) + ", " + str(nr_points + 5) + "};\n" \
              "Line(15) = {" + str(nr_points + 5) + ", " + str(nr_points + 9) + "};\n" \
              "Line(16) = {" + str(nr_points + 9) + ", " + str(nr_points + 10) + "};\n" \
              "Line(17) = {" + str(nr_points + 11) + ", " + str(nr_points + 12) + "};\n" \
              "Line(18) = {" + str(nr_points + 11) + ", " + str(nr_points + 10) + "};\n" \
              "Line(19) = {" + str(nr_points + 8) + ", " + str(nr_points + 11) + "};\n" \
              "Line(20) = {" + str(nr_points + 8) + ", " + str(nr_points + 7) + "};\n" \
              "Line(21) = {" + str(nr_points + 8) + ", " + str(nr_points + 9) + "};\n" \
              "Line(22) = {1, " + str(nr_points + 8) + "};\n" \
              "Line(23) = {1, " + str(nr_points + 6) + "};\n" \
              "Line(24) = {1, " + str(nr_points + 5) + "};\n" \
              "Line(25) = {" + str(nr_points + 4) + ", " + n3 + "};\n"
       mesh_file.writelines(body)

       body = "\n// Mesh parameters\n" \
              "n_inlet = " + str(n_inlet) + ";\n" \
              "n_outlet = " + str(n_outlet) + ";\n" \
              "n_airfoil = " + str(n_airfoil) + ";\n" \
              "n_wake_short = " + str(n_wake_short) + ";\n" \
              "n_wake_long = " + str(n_wake_long) + ";\n" \
              "\nTransfinite Curve {-2,3} = n_inlet Using Progression 1.09;\n" \
              "Transfinite Curve {7,8} = n_inlet Using Progression 1.09;\n" \
              "Transfinite Curve {22,-9, 24, 21, 18, 20, 17, -25, -10, -28, 23} = n_outlet Using Progression 1.07;\n" \
              "Transfinite Curve {22} = n_wake_short Using Progression 1.07;\n" \
              "Transfinite Curve {12,15} = n_wake_short Using Bump 1.5;\n" \
              "Transfinite Curve {19} = n_wake_long Using Progression 1.03;\n" \
              "Transfinite Curve {13, 16} = n_wake_long Using Progression 1.05;\n" \
              "Transfinite Curve {4,5} = n_airfoil Using Bump 0.3;\n" \
              "Transfinite Curve {11, 14} = n_airfoil Using Bump 4;\n"
       mesh_file.writelines(body)

       body = "\nCurve Loop(1) = {7, 25, -2, -9};\n" \
              "Plane Surface(1) = {1};\n" \
              "Curve Loop(2) = {25, 3, -10, 8};\n" \
              "Plane Surface(2) = {2};\n" \
              "Curve Loop(3) = {9, -5, 23, -11};\n" \
              "Plane Surface(3) = {3};\n" \
              "Curve Loop(4) = {10, 4, 24, -14};\n" \
              "Plane Surface(4) = {4};\n" \
              "Curve Loop(5) = {23, 12, -20, -22};\n" \
              "Plane Surface(5) = {5};\n" \
              "Curve Loop(6) = {24, 15, -21, -22};\n" \
              "Plane Surface(6) = {6};\n" \
              "Curve Loop(7) = {20, 13, -17, -19};\n" \
              "Plane Surface(7) = {7};\n" \
              "Curve Loop(8) = {21, 16, -18, -19};\n" \
              "Plane Surface(8) = {8};\n" \
              "\nTransfinite Surface {1};\n" \
              "Transfinite Surface {2};\n" \
              "Transfinite Surface {3};\n" \
              "Transfinite Surface {4};\n" \
              "Transfinite Surface {5};\n" \
              "Transfinite Surface {6};\n" \
              "Transfinite Surface {7};\n" \
              "Transfinite Surface {8};\n" \
              "\nRecombine Surface {1, 2, 3, 4, 5, 6, 7, 8};\n" \
              "\nExtrude {0, 0, 1} {Surface{1}; Surface{2}; Surface{3}; Surface{4}; Surface{5}; Surface{6}; Surface{7}; Surface{8}; Layers{1}; Recombine;}\n" \
              "\nPhysical Volume(\"Fluid\") = {1, 2, 3, 4, 5, 6, 7, 8};\n" \
              "Physical Surface(\"Inlet\") = {34, 68,90, 126, 170, 112, 148, 192};\n" \
              "Physical Surface(\"Outlet\") = {174, 196};\n" \
              "Physical Surface(\"Sides\") = {47, 69, 2, 91, 3, 5, 135, 179, 7, 201, 8, 157, 6, 113, 4, 1};\n" \
              "Physical Surface(\"Airfoil\") = {82, 104, 42, 60};"
       mesh_file.writelines(body)

       mesh_file.close()

if __name__ == '__main__':
       # user input
       airfoil_name = input("Enter airfoil (file) name:")
       # airfoil_name = 'JF254110_V.1'  # for debugging
       # alpha = input("Enter angle of attack (in degrees):")
       alpha = 0  # for debugging

       default_options = 1;
       temp = input("Default mesh options [y/n]?")
       if temp=='n':
           default_options=0;
       service_func(airfoil_name, alpha, default_options)

