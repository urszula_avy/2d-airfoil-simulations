# Quick conversion of airfoil coordinates from XFLR .dat to Gmsh compatible .geo file
# Urszula Golyska 2021 c

def coordinate_conversion(airfoil_name):
    # user input
    # airfoil_name = input("Enter airfoil (file) name:")
    # airfoil_name = 'JF254110_V.1'

    # reading from file
    xflr_file = open(airfoil_name + ".dat", 'r')
    count = -1
    gmsh_file = open(airfoil_name + ".geo", 'w')

    # Strips the newline character
    for line in xflr_file:
        if count == -1:
            count = 0
        else:
            count += 1
            coord = line.split()
            if count == 1:
                coord_first_point = coord
            if not count == len(open(
                    airfoil_name + ".dat").readlines()) - 1:  # because we need a sharp trailing edge, we omit the last point
                # print("Point(" + str(count) + ") =  {" + str(coord[0]) + ", " + str(coord[1]) + ", 0, 1.0000000};")
                converted = "Point(" + str(count) + ") =  {" + str(coord[0]) + ", " + str(
                    coord[1]) + ", 0, 1.0000000};\n";

                # writing to file
                gmsh_file.writelines(converted)

    # add point to close loop
    # count += 1
    # print("Point("+ str(count) + ") =  {" + str(coord_first_point[0]) + ", " + str(coord_first_point[1]) + ", 0, 1.0000000};")
    # converted = "Point("+ str(count) + ") =  {" + str(coord_first_point[0]) + ", " + str(coord_first_point[1]) + ", 0, 1.0000000};\n";
    # gmsh_file.writelines(converted)

    # add line
    points = str(list(range(1, count)))
    create_line = "Line (1) = {" + points[1:-1] + ", 1};"
    # print(str(create_line))
    gmsh_file.writelines(create_line)

    xflr_file.close()
    gmsh_file.close()

if __name__ == '__main__':
    coordinate_conversion(airfoil_name)



