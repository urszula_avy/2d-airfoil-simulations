# Script for changing inlet conditions for 2D airfoil simulation
# Urszula Golyska 2021 c
from distutils.dir_util import copy_tree
import specInlet_multi as sp

def loop_func(U, alpha, dir_name):
       # copy subdirectory
       copy_tree('basic', dir_name)

       # change settings
       sp.service_func(U, alpha, dir_name)
if __name__ == '__main__':
       # user input
       U = float(input("Enter freestream velocity (in m/s):"))
       alpha_start = float(input("Enter lowest angle of attack (in degrees):"))
       alpha_end = float(input("Enter highest angle of attack (in degrees):"))
       alpha_inc = float(input("Enter angle of attack increment (in degrees):"))

       alpha_array = [alpha_start + alpha_inc * interval for interval in range(int((alpha_end - alpha_start)/alpha_inc)+1)]

       # create all run file
       run_file = open("Allrun_cases", 'w')
       run_file.writelines('# !/bin/bash\n\n')

       for alpha in alpha_array:
              dir_name = "U_" + str(U) + "_aoa_" + str(alpha)
              loop_func(U,alpha,dir_name)
              run_file.writelines("simpleFoam -case "+str(dir_name)+"> log"+str(dir_name)+".log \n")
